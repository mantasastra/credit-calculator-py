import math
import argparse


class CreditCalculator:
    def __init__(self):
        self.principal = 0
        self.payment = 0
        self.diff_payments = []
        self.periods = 0
        self.interest = 0
        self.nominal_interest = 0

    def calculate_principal(self, payment, periods, interest):
        self.payment = payment
        self.periods = periods
        self.interest = interest

        self.calculate_nominal_interest_rate()

        self.principal = math.floor(self.payment
                                    / ((self.nominal_interest * math.pow(1 + self.nominal_interest, self.periods))
                                       / (math.pow(1 + self.nominal_interest, self.periods) - 1)))

        print(f"Your credit principal = {self.principal}!")

    def calculate_annuity_payment(self, principal, periods, interest):
        self.principal = principal
        self.periods = periods
        self.interest = interest

        self.calculate_nominal_interest_rate()

        self.payment = math.ceil((self.principal
                                  * ((self.nominal_interest * math.pow(1 + self.nominal_interest, self.periods))
                                     / (math.pow(1 + self.nominal_interest, self.periods) - 1))))

        print(f"Your annuity payment = {self.payment}!")

    def calculate_differentiated_payment(self, principal, periods, interest):
        self.principal = principal
        self.periods = periods
        self.interest = interest

        self.calculate_nominal_interest_rate()

        for month in range(1, self.periods + 1):
            payment = math.ceil((self.principal / self.periods)
                                + self.nominal_interest
                                * (self.principal - ((self.principal * (month - 1)) / self.periods)))

            self.diff_payments.append(payment)

            print(f"Month {month}: paid out {payment}")

    def calculate_months(self, principal, payment, interest):
        self.principal = principal
        self.payment = payment
        self.interest = interest

        self.calculate_nominal_interest_rate()

        self.periods = math.ceil(math.log(self.payment
                                          / (self.payment - self.nominal_interest * self.principal),
                                          1 + self.nominal_interest))

        years = math.floor(self.periods / 12)
        months = self.periods % 12

        if years > 0 and months > 0:
            print(f"You need {years} year{'s' if years != 1 else ''} and"
                  f" {months} month{'s' if months != 1 else ''} to repay this credit!")
        elif years == 0 and months > 0:
            print(f"You need {months} month{'s' if months != 1 else ''}"
                  f" to repay this credit!")
        elif years > 0 and months == 0:
            print(f"You need {years} year{'s' if years != 1 else ''}"
                  f" to repay this credit!")

    def calculate_nominal_interest_rate(self):
        self.nominal_interest = self.interest / (12 * 100)

    def calculate_overpayment(self, calc_type):
        total_paid = None

        if calc_type == 'annuity':
            total_paid = self.payment * self.periods
        elif calc_type == 'diff':
            total_paid = sum(self.diff_payments)

        overpayment = math.ceil(total_paid - self.principal)

        print(f"\nOverpayment = {overpayment}")

    @staticmethod
    def parse_arguments():
        parser = argparse.ArgumentParser()

        parser.add_argument("--type", type=str)
        parser.add_argument("--principal", type=int)
        parser.add_argument("--periods", type=int)
        parser.add_argument("--interest", type=float)
        parser.add_argument("--payment", type=float)

        args = parser.parse_args()

        return args

    def run(self):
        args = self.parse_arguments()

        if args.type == 'annuity' and args.interest is not None:
            if args.payment and args.periods and args.interest:
                self.calculate_principal(args.payment, args.periods, args.interest)
            elif args.principal and args.payment and args.interest:
                self.calculate_months(args.principal, args.payment, args.interest)
            elif args.principal and args.periods and args.interest:
                self.calculate_annuity_payment(args.principal, args.periods, args.interest)
        elif args.type == 'diff' and args.payment is None:
            self.calculate_differentiated_payment(args.principal, args.periods, args.interest)
        else:
            print("Incorrect parameters")
            exit()

        self.calculate_overpayment(args.type)


credit_calculator = CreditCalculator()
credit_calculator.run()
